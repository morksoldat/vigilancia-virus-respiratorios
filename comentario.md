La información de síndromes respiratorios corresponde a la semana 21 en 3 Centros Centinelas. Las consultas por síndromes respiratorios aumentaron importantemente a 7% del total de consultas mostrando estabilidad en las consultas por esta causa. Se observó predominio y aumento de consultas por SBO (2,7%) y laringitis aguda (2,4%) en concordancia con el brote de parainfluenza. Destaca la curva de ascenso de las consultas de ETI a 2% y la fiebre faringoconjuntival se mantiene muy baja en 0%. (gráficos 6 y 8).

El porcentaje total de positividad de las muestras estudiadas es de 30% (138/458).

En la Región Metropolitana continúa predominando parainfluenza dentro de los virus tradicionales, el cual mostró un aumento con respecto a las semanas previas. Sin embargo se observa un aumento continuo y significativo de VRS, al igual que de influenza A, ocupando el segundo y tercer lugar respectivamente. Hay circulación importante de adenovirus y aumentó la circulación de metaneumovirus. (gráfico 1)

En los virus no tradicionales, persiste el predominio de rinovirus/enterovirus observándose una disminución de la circulación de coronavirus. (gráfico 2)

En Puerto Montt la circulación es prácticamente solo VRS, el que continúa en ascenso y ha comenzado a aparecer influenza A en las últimas dos semanas, mostrando un incremento. (gráfico 3)

En Osorno es evidente el brote estacional de VRS, superando a rinovirus. En tercer lugar, pero bastante menos frecuente se encuentra parainfluenza (gráfico 4)

Durante esta semana en los grupos etarios: < 1 año y   1 y 14 años predomina parainfluenza con 45% y 33% respectivamente. En el grupo > de 14 años predomina influenza A con 41% . (gráfico 5)
