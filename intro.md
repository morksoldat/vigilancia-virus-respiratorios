# Virus Respiratorios UC

Bienvenido al portal de Virus Respiratorios de la Pontificia Universidad Católica de Chile

El propósito del Proyecto de Vigilancia de Virus Respiratorios es entregar información actualizada de los virus respiratorios circulantes en Santiago, que pueda ser útil para los médicos en su práctica clínica, el personal de salud y público general.

## Instituciones Participantes

* Servicio Urgencia del,Hospital de la Universidad Católica
* Unidad de Pediatría Centro Médico San Joaquín
* Facultad de Medicina Clínica Alemana - Universidad del Desarrollo
* Exámenes de Laboratorio SA. (Integramédica)
