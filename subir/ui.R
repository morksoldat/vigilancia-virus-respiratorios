fluidPage(
          navbarPage("Ingreso de Datos", id="hello",
                    # tabPanel("Ingreso", br(), h3("Contraseña"),passwordInput("pass", "Ingrese la contraseña para entrar al sistema:"),actionButton("enter", "enter")),
                     tabPanel("Comentarios", value = "tab2_val",
                              sidebarLayout(
                                sidebarPanel(
                                  selectInput("año","Año", format(Sys.Date(), "%Y"):2016),
                                  selectInput("se","Semana Epidemiológica", 1:53, selected = as.integer(as.week(Sys.Date())$week)),
                                  radioButtons("sobresc", "Sobre escribir datos previos:",
                                               c("No" = "no",
                                                 "Si" = "si"))
                                ),
                                mainPanel(
                                  h4("Comentario"),
                                  p("A continuación ingresa el comentario de la semana"),
                                  textAreaInput("comentario", "Comentario", width = "100%", cols = 1000),
                                  actionButton("enviar_com","Enviar")
                                ))),
                     tabPanel("Archivos", value = "tab3_val", 
                              br(), h1("Subir archivos de virus"),
                              p("El archivo debe estar en fromato CSV y contener las siguientes columnas en el siguiente orden:"),
                              p(div(HTML("<ul><li><b>ID</b>: Identificador, puede ser cualquier número o cadena que identifique al paciente como N° órden, correlativo, N° fícha. <b>No puede ser el rut</b></li>
                                      <li><b>Fecha de nacimiento:</b> En formato dd/mm/AAAA, ej 23/06/1912</li>
                                      <li><b>Fecha de toma de muestra:</b> En formato dd/mm/AAAA, ej 19/05/2018</li>
                                      <li><b>Virus:</b> Virus encontrado. Se aceptan como variables válidas las contenidas en este archivo <b>LINK</b>.</li>
                                      </ul>"))),
                              selectInput("centro", "Centro", c("Red UC" = "uc", "Clínica Alemana" = "cas", "Integramédica" = "im", 
                                                                "H. Lucio Córdova" = "lucio", "Osorno" = "osorno", "Pto. Montt" = "pmontt")),
                              radioButtons("subir_tipo", "Tipo de datos", c("Genérico" = "g" , "Modulab" = "m"), inline=T),
                              fileInput("subir", "Subir CSV", accept=c("text/csv", "text/comma-separated-values,text/plain", ".csv")),
                              h4("Pre vizualización de los datos:"),
                              tableOutput("filetable"),
                              actionButton("enviar_csv","Enviar")
                     )))